// 2021-09-03 Alexander Dimitrov
// Main Mapping File v0.6
map "http://hl7.org/fhir/StructureMap/generate-ips-main" = "generate-ips-main"

uses "http://hl7.org/fhir/cda/StructureDefinition/ClinicalDocument" alias ClinicalDocument as source
uses "http://hl7.org/fhir/cda/StructureDefinition/Section" alias Section as queried
uses "http://hl7.org/fhir/cda/StructureDefinition/PatientRole" alias PatientRole as queried
uses "http://hl7.org/fhir/StructureDefinition/Bundle" alias Bundle as target
uses "http://hl7.org/fhir/StructureDefinition/Composition" alias Composition as produced
uses "http://hl7.org/fhir/StructureDefinition/Patient" alias Patient as produced

imports "http://hl7.org/fhir/StructureMap/generate-ips-sections"
imports "http://hl7.org/fhir/StructureMap/concept-maps-elga2ips" 

group generateCompositionPatient(source cda : ClinicalDocument,  target bundle : Bundle) {
    cda ->  bundle.entry as entry, 
            entry.resource = create('Composition') as composition, 
            composition.id = uuid() as uuid,
            entry.fullUrl = append('urn:uuid:',uuid),
            bundle.entry as entry2, 
            entry2.resource = create('Patient') as patient,
            patient.id = uuid() as uuid2, 
            entry2.fullUrl = append('urn:uuid:',uuid2) then {
                cda then generateBundleElements(cda, patient, composition, bundle) "generateBundleElements1";
            } "generateBundleElements2";
  }

group generateBundleElements(source cda : ClinicalDocument, target patient : Patient, target composition : Composition, target bundle : Bundle) {
    cda -> bundle.id = uuid() "bundle-id";
    cda -> bundle.type = 'document' "bundle-type"; // die Ressourcen werden nicht einzeln hochgeladen, sondern nur als ein Document-Bundle (alternativ -> batch / transaction)
    cda -> bundle.identifier as identifier, identifier.value=uuid(), identifier.system='urn:oid:SYSTEM-TO-BE-DEFINED' "bundle-identifier"; // Platzhalter
    cda.effectiveTime -> bundle.timestamp = (now()) "bundle-assembling-time";
    cda then generateCompositionElements(cda, composition, patient, bundle) "generateCompositionElements";
}

group generateCompositionElements(source src : ClinicalDocument, target tgt : Composition, target patientResource: Patient, target bundle : Bundle) {
    
    // Sprache
    src.languageCode as languageCode -> tgt.language as language then {
        languageCode.code as code -> language.value = cast(code, 'string');
    }"composition-language";
    
    // Datum
    src.effectiveTime -> tgt.date = (now());

    // Status, Typ und Titel
    src -> tgt.status = 'preliminary' "composition-status";
    src -> tgt.type as type, type.coding as coding, coding.system = 'http://loinc.org', coding.code = '60591-5', coding.display = 'Patient summary Document' "composition-type-coding";
    src -> tgt.title = 'International Patient Summary' "composition-title";
    
    // Patient
    src.recordTarget as recordTarget then {
        recordTarget.patientRole as patient -> tgt.subject = create('Reference') as reference, reference.reference = ('urn:uuid:'+%patientResource.id) then generatePatient(patient, src, patientResource, bundle) "composition-subject";
    }"composition-patient-reference";

    // Autor
    src ->  bundle.entry as entry,  
            entry.resource = create('Device') as device, 
            device.id = uuid() as uuid2,
            entry.fullUrl = append('urn:uuid:',uuid2),
            tgt.author = create('Reference') as reference, reference.reference = ('urn:uuid:'+%device.id) then {
                src -> device.text as text then {
                    src -> text.status = 'additional' "device-narrative-status";
                    src -> text.div = '<div xmlns="http://www.w3.org/1999/xhtml"><p>This Device generates an IPS FHIR Document from in production used ELGA CDA</p></div>' "device-narrative-text";
                }"narrative-device";
                src -> device.manufacturer = '<INSERT MANUFACTURER NAME HERE>' "manucaturer-name";
                src -> device.deviceName as deviceName then {
                    src -> deviceName.name = 'IPS Generator' "deviceName-name";
                    src -> deviceName.type = 'user-friendly-name' "deviceName-type";
                }"deviceName";
            }"composition-author";   

      // IPS Problems Section generieren
      src as src2 where (component.structuredBody.component.section.entry.act.entryRelationship.observation.exists()) 
      then generateActiveProblemsSection(src, tgt, patientResource, bundle) "problems-section";
      
      // IPS Vital Signs Section generieren
      src as src2 where (component.structuredBody.component.section.component.section.entry.organizer.exists()) 
      then generateVitalSignsSection(src, tgt, patientResource, bundle) "vital-signs-section";
      
      // IPS Medication Section generieren
      src as src2 where (component.structuredBody.component.section.entry.supply.product.manufacturedProduct.manufacturedMaterial.exists()) 
      then generateMedicationSummarySection(src, tgt, patientResource, bundle) "medication-summary-section";

      // IPS Laboratory Results Section generieren
      src as src2 where (component.structuredBody.component.section.entry.act.entryRelationship.organizer.component.observation.code.exists()) 
      then generateResultsSection(src, tgt, patientResource, bundle) "results-section";

      // IPS Allergies and Intolerances Section (via Narrative) generieren
      src as src2 where (component.structuredBody.component.section.templateId.where(root='1.2.40.0.34.11.2.2.13'))
      then generateAllergyIntoleranceSection(src, tgt, patientResource, bundle) "allergyIntolerance-section";

      // IPS Allergies and Intolerances Section (no known allergies) generieren
      src first as src2 where (component.structuredBody.component.section.templateId.where(root='1.2.40.0.34.11.2.2.13').exists().not())
      then generateNoKnownAllergyIntoleranceSection(src, tgt, patientResource, bundle) "allergyIntolerance-noKnown-section";

      // IPS Immunizations Section generieren
      src as src2 where (component.structuredBody.component.section.entry.substanceAdministration.templateId.where(root='1.2.40.0.34.6.0.11.3.1').exists())
      then generateImmunizationSection(src, tgt, patientResource, bundle) "immunizations-section";

  }

  group generatePatient(source src : PatientRole, source src2 : ClinicalDocument, target tgt : Patient, target bundle : Bundle) {
    
    // Patient.identifier
    src.id as id where (assigningAuthorityName.endsWith('Sozialversicherung')) then { 
        id -> tgt.identifier as identifier then {
            id.root as root -> identifier.use = 'usual', 
                               identifier.system = 'urn:oid:1.2.40.0.10.1.4.3.1',
                               identifier.type as type, type.coding as coding,
                               coding.system = 'http://terminology.hl7.org/CodeSystem/v2-0203',
                               coding.code = 'SS',
                               coding.display = 'Social Security Number',
                               identifier.assigner as assigner, 
                               assigner.display = 'Dachverband der österreichischen Sozialversicherungsträger'; 
            id.extension as extension -> identifier.value = extension "extension"; 
        } "patient-id";
    } "patient-root-extension";

    // Patient.address
    src.addr as addr then {
        addr -> tgt.address as address then {
            addr.use as use -> address.use = translate(use, 'concept-maps-elga2ips#address-use', 'code') "patient-address-use";
            addr.streetAddressLine as line2 -> address.line = (line2.dataString) "patient-address-line";
            addr.city as city -> address.city = (city.dataString) "patient-address-city";
            addr.state as state -> address.state = (state.dataString) "patient-address-state";
            addr.postalCode as postalCode -> address.postalCode = (postalCode.dataString) "patient-address-postalCode";
            addr.country as country -> address.country = (country.dataString) "patient-address-country";
            // text from multiple elements (optional)
            //addr.city as city -> address.text = ('test'+%city.dataString+' d '+%city.dataString) "addr-as-text";
            //addr -> address.text = ('test'+%line.dataString+' '+%postalCode.dataString+' '+%city.dataString+' '+%country.dataString) "addr-as-text";
        }"patient-address";
    }"patient-address-elements";

    // Patient.telecom
    src.telecom as telecom -> tgt.telecom as telecom2 then {
        telecom.value as value where (telecom.value.startsWith('tel:')) ->  telecom2.value = (value.substring(4)),  telecom2.system = 'phone' "patient-telecom-value-tel";
        telecom.value as value where (telecom.value.startsWith('fax:')) ->  telecom2.value = (value.substring(4)),  telecom2.system = 'fax' "patient-telecom-value-fax";
        telecom.value as value where (telecom.value.startsWith('mailto:')) ->  telecom2.value = (value.substring(7)),  telecom2.system = 'email' "patient-telecom-value-mail";
        telecom.value as value where (telecom.value.startsWith('http:')) ->  telecom2.value = (value.substring(5)),  telecom2.system = 'url' "patient-telecom-value-http";
        telecom.use as use -> telecom2.use = translate(use, 'concept-maps-elga2ips#telecom-use', 'code') "patient-telecom-use";
    }"telecom"; 

    
    src.patient as patient then {
        // Name
        patient.name as name -> tgt.name as name2 then {
            name.family as family where (qualifier.exists().not()) -> name2.family = (family.dataString) "patient-name-family";
            name.given as given -> name2.given = (given.dataString) "patient-name-given";
            name.prefix as prefix -> name2.prefix = (prefix.dataString) "patient-name-prefix";
            name.suffix as given -> name2.suffix = (suffix.dataString) "patient-name-suffix";
        } "patient-name"; 

        // Geschlecht 
        patient.administrativeGenderCode as gender then {
            gender.displayName as displayName -> tgt.gender = translate(displayName, 'concept-maps-elga2ips#administrativeGender', 'code') "mapping-adadministrativeGender";
        }"patient-administrativeGender";
      
        // Geburtsdatum
        patient.birthTime as birthTime -> tgt.birthDate = (birthTime.value) "patient-birthDate";

        // Mehrfachgeburt
        patient.multipleBirthInd as multipleBirthInd -> tgt.multipleBirthBoolean = (multipleBirthInd.value) "patient-multipleBirthInd"; 
        patient.multipleBirthOrderNumber as multipleBirthOrderNumber -> tgt.multipleBirthInteger = (multipleBirthOrderNumber.value) "patient-multipleBirthOrderNumber"; 

        // Tod
        patient.deceasedInd -> tgt.deceaseedBoolean;
        patient.deceasedTime -> tgt.deceasedDateTime;
      
        // Ehestand
        patient.maritalStatusCode as mcode -> tgt.maritalStatus as martialStatus then {
            mcode -> martialStatus.coding as mcoding then {
                mcode -> mcoding.code = (mcode.code) "patient-martial-code";  // no concept mapping needed, since all ELGA codes are included in the IPS ValueSet
                mcode-> mcoding.display = (mcode.displayName) "patient-martial-display";
                mcode.codeSystem as codeSystem -> mcoding.system = translate(codeSystem, 'concept-maps-elga2ips#oid-to-system-url', 'code') "patient-martial-codeSystem";
            }"patient-martialStatus-coding"; 
        }"patient-martialStatus"; 

        // Relgion (Erweiterung aus AustrianPatient Profil) 
        patient.religiousAffiliationCode as religiousAffiliationCode -> tgt.extension as extension then {
            religiousAffiliationCode -> extension.url = 'http://hl7.at/fhir/HL7ATCoreProfiles/4.0.1/StructureDefinition/at-core-ext-patient-religion' then {
                religiousAffiliationCode -> extension.extension as extension2, extension2.url = 'code' ,extension2.value = create('CodeableConcept') as CodeableConcept, CodeableConcept.coding as coding then {
                    religiousAffiliationCode.code as code -> coding.code = (code) "patient-relegion-code";
                    religiousAffiliationCode.displayName as displayName -> coding.display = (displayName) "patient-relegion-display";
                    religiousAffiliationCode.codeSystem as codeSystem -> coding.system = ('urn:oid:'+(codeSystem)) "patient-relegion-system";
                }"patient-relegion-extension2-code";
            }"patient-relegion-extension";

        }"patient-relegion"; 


        // Kommunikation
        patient.languageCommunication as language -> tgt.communication as communication then {
            language.languageCode as languageCode -> communication.language as language then {
                languageCode.code as code -> language.coding as coding,
                                            coding.code as code2, 
                                            coding.system = 'urn:ietf:bcp:47',
                                            code2.value = cast(code, 'string') "patient-languageCode-coding"; 
            }"patient-communication-language";
            language.preferenceInd as preferenceInd -> communication.preferred =  (preferenceInd.value) "patient-language-preferenceInd"; 
        }"patient-language"; 

    } "patient";

    // Kontakt  
    src2.participant as participant -> tgt.contact as contact then {
        participant.associatedEntity as associatedEntity -> contact.relationship as relationship, contact.address as address, contact.telecom as telecom2, contact.name as contactname then {
            associatedEntity.code as code where (associatedEntity.classCode='ECON') -> relationship.coding as coding then {
                code -> coding.code = (code.code), coding.display = (code.displayName) "patient-contact-coding1";
                code.codeSystem as codeSystem -> coding.system = translate(codeSystem, 'concept-maps-elga2ips#oid-to-system-url', 'code') "patient-contact-coding2";
            }"patient-contact-code"; 
            associatedEntity.addr as addr where (associatedEntity.classCode='ECON') -> address then {
                addr.city as city  -> address.city = (city.dataString)  "patient-contact-city";
                addr.streetName as streetName then { 
                    addr.houseNumber as houseNumber -> address.line = (%streetName.dataString+' '+%houseNumber.dataString) "patient-contact-street-houseNumber";
                }"patient-contact-line";
                addr.postalCode as postalCode -> address.postalCode = (postalCode.dataString) "patient-contact-postalCode";
                addr.country as country -> address.country = (country.dataString) "patient-contact-country";
            }"patient-contact-address"; 

            associatedEntity.telecom as telecom where (associatedEntity.classCode='ECON') -> telecom2 then {
                telecom.value as value where (telecom.value.startsWith('tel:')) ->  telecom2.value = (value.substring(4)),  telecom2.system = 'phone' "patient-contact-value-tel";
                telecom.value as value where (telecom.value.startsWith('fax:')) ->  telecom2.value = (value.substring(4)),  telecom2.system = 'fax' "patient-contact-value-fax";
                telecom.value as value where (telecom.value.startsWith('mailto:')) ->  telecom2.value = (value.substring(7)),  telecom2.system = 'email' "patient-contact-value-mail";
                telecom.value as value where (telecom.value.startsWith('http:')) ->  telecom2.value = (value.substring(5)),  telecom2.system = 'url' "patient-contact-value-http";
                telecom.use as use -> telecom2.use = translate(use, 'concept-maps-elga2ips#telecom-use', 'code') "use";
            }"patient-contact-telecom";
            associatedEntity.associatedPerson as associatedPerson where (associatedEntity.classCode='ECON') -> contactname then {
                associatedPerson.name as name then {
                    name.given as given -> contactname.given as given2, given2.value = (given.dataString) "patient-contact-given";
                    name.family as family where (qualifier.exists().not()) -> contactname.family as family2, family2.value = (family.dataString) "patient-contact-family";
                }"patient-contact-name";
            }"patient-contact-associatedPerson";
        }"patient-contact-associatedEntity"; 
    }"patient-contact";  
    
  }


  