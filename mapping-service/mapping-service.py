## 2021-09-01 Alexander Dimitrov
## Mapping Service v2.0

import requests
import xml.etree.ElementTree as ET
import os, re, os.path
from array import array
from lxml import objectify, etree
from os.path import exists
from requests.api import post

######### Konfiguration ######### 

# Endpunkte FHIR Server
fhir_store_endpoints = ['http://hapi.fhir.org/baseR4','http://localhost:8080/fhir', 'https://server.fire.ly', 'http://localhost:8080', 'https://server.fire.ly/r4']
mapping_engine_endpoints = ['https://test.ahdis.ch/r4','http://localhost:8080/r4']
url_storage = fhir_store_endpoints[0] # URL zum FHIR Store (0 öffentlich, 1 lokal)
url_mapping = mapping_engine_endpoints[1] # URL zur Mapping Engine (0 öffentlich, 1 lokal)

# Validierung und Verifikation
patient_verification = True # Schalter, um zu überprüfen, ob die verwendeten Dokumente zum selben Patienten gehören (True / False)
bundle_validation = False # generiertes Bundle gegen IPS Bundle Profil validieren (True / False)

# Bundle-Konfig
post_bundle = True # Schalter, ob das IPS im FHIR Store gespeichert werden soll - bei False nur lokal (../genereratedIpsBundle.xml) (True / False)
update_bundle = False # Schalter, um ein Bundle Update zu erlauben (falls True, muss der Benutzer die ID des zu aktualisierenden Pakets eingeben) (True / False)

# Andere
post_maps = True # Schalter für Map Upload (True / False)
post_profiles = 'bundle' # 'bundle', falls das IPS Bundle Profil hochgeladen werden soll, sonst 'no' 
store_bundleId = False # Falls 'True' wird die Bundle-ID aus dem hochgeladenen Bundle in einer .txt-Datei gespeichert (True / False)
ns = {'fhir':'http://hl7.org/fhir','hl7':'urn:hl7-org:v3'} # verwendete namespaces

######### CDA XML Merger v1.3 ######### 

# Ordnerpfad von Quell-XMLs 
path = os.path.dirname(__file__)
path = os.path.join(path, 'input-xmls')

# Verzeichnis ändern
os.chdir(path)

# Arrays deklarieren, in welchen die CDAs und die CDA-Dateinamen gespeichert werden 
readFiles = []
fileNames = []

# Jedes XML in ein Array laden
i = 0 # counter
print ("Following CDA XMLs have been imported: ")
for root, dirs, files in os.walk(path):
    for file in files:
        i = i + 1
        print ("Document " + str(i) + ": " + file)
        filename, extension = os.path.splitext(file)
        if extension == '.xml' and ('ELGA-' or 'eImpf-') in filename: #TODO eImpf bug
            readFiles.append( open(file, "r"))
            fileNames.append(filename+extension) # wird für die Patientenverifikation verwendet

print ("Total documents imported: " + str(len(readFiles)))
print ("--------------------------------------------------------------------------")

# CDA-Hauptdokument auswählen
print ("Select from which CDA Document the CDA Header should be used (example: 1):")
main_cda_number = input() 
main_cda_number = int(main_cda_number)

# CDA-Hauptdokument laden
main_cda_tree = etree.parse(readFiles[main_cda_number-1])
main_cda_root = main_cda_tree.getroot()

print ((readFiles[main_cda_number-1].name) + " has been selected as the main CDA")
print ("--------------------------------------------------------------------------")

# Konvertierte xmls aus vorheriger Konvertierung entfernen 
convertedFilesPath = "../converted-xmls"
for root, dirs, files in os.walk(convertedFilesPath):
    for file in files:
        os.remove(os.path.join(root, file))


# Funktion zum Speichern von Patienten-IDs (Sozialversicherungsnummern) aus allen verwendeten Dokumenten in einem Array 
def verifyPatient(cda_path):
    patient_tree = etree.parse(cda_path) # parse CDA 
    patient_root = patient_tree.getroot() # get ClinicalDocument root
    patientIds.append( patient_root.xpath('//hl7:recordTarget/hl7:patientRole/hl7:id/@extension', namespaces=ns)[1]) # SVNR einsentzen

# verifyPatient wird ausgeführt, um zu prüfen, ob alle CDA-Dokumente zu gleichen Patienten gehören.  (optional -> Konfiguration via patient_verification)
if patient_verification == True:
    patientIds = [] # Array zur Speicherung der Patienten-IDs aus den CDA-Dokumenten
    i = 0 # counter
    for cdaToBeVerified in readFiles:
        #verifyPatient(cdaToBeVerified) # alternative
        verifyPatient("../input-xmls/"+fileNames[i])
        i = i + 1
    #print (patientIds) # für testzwecke
    
    # prüft, ob alle IDs aus dem Array identisch sind
    if all(x == patientIds[0] for x in patientIds): 
        print ("Note: All documents belong to the same patient")
    else:
        print ("Warning: Not all documents belong to the same patient")
else: 
    print('Note: Patient verification for document ownership was not executed')

# Funktion zum Entfernen von Header-Tags aus den CDAs (außer für das Haupt-CDA)
def simplifyCda(cda):
    cda_tree = etree.parse(cda) # parse CDA für Transformation
    cda_root = cda_tree.getroot() # get ClinicalDocument root

    # Definition, welche Header-Elemente aus dem CDA gelöscht werden sollen
    headerTagsToBeRemoved = ['hl7:realmCode','hl7:templateId','hl7:typeId','hl7:code','hl7:title','hl7:effectiveTime',
                            'hl7:id','hl7:confidentialityCode','hl7:languageCode','hl7:setId','hl7:versionNumber',
                            'hl7:recordTarget','hl7:author','hl7:dataEnterer','hl7:custodian','hl7:informationRecipient',
                            'hl7:legalAuthenticator','hl7:authenticator','hl7:participant','hl7:documentationOf',
                            'hl7:componentOf']

    # Definierte Elemente aus dem CDA entfernen
    for tagToBeRemoved in headerTagsToBeRemoved:
        for node in cda_tree.xpath(tagToBeRemoved, namespaces=ns):
            node.getparent().remove(node)

    # Alle alten Kommentare aus dem Dokument entfernen
    comments = cda_tree.xpath('//comment()')
    for c in comments:
        p = c.getparent()
        p.remove(c)

    # Kommentar zu konvertiertem CDA hinzufügen
    commentCda = etree.Comment(' === This part will be merged into the main cda === ')
    cda_root.insert(0, commentCda)
    
    # Konvertierte Datei speichern
    convertedCdaFileName = "../converted-xmls/" + "converted:" + (cda.name)
    cda_tree.write(convertedCdaFileName)

    # component/structuredBody-Elemente in Haupt-CDA hinzufügen
    for node in cda_tree.xpath('//hl7:component/hl7:structuredBody', namespaces=ns):
        main_cda_root.append(node.getparent())
    
    ### ENDE simplifyCda Funktion      

# simplifyCda für alle CDA ausführen (nur das Haupt-CDA wird nicht konvertiert)
i = 0 # counter
for cdaToBeSimplified in readFiles:
    i = i + 1
    if i != main_cda_number:
        simplifyCda(cdaToBeSimplified)

# XML aus allen merged CDA in eine Datei schreiben
main_cda_tree.write('../prepared-xml-for-ips/mergedCdasForPatientSummary.xml')

######### HTTP Requests ######### 

# Die aktuellsten Maps auf Mapping Server hochladen (optional -> Konfiguration via post_maps, url_mapping and url_storage) 
if post_maps == True:
    url_maps = url_mapping + '/StructureMap' 
    headers_maps = {'Accept': 'application/fhir+xml;fhirVersion=4.0', 'Content-type': 'text/fhir-mapping'}
    path2 = os.path.dirname(__file__) # absoluter Pfad des Verzeichnisses, in dem sich das Skript befindet
    rel_path_maps = ['../maps/generate-ips-main.map','../maps/generate-ips-sections.map','../maps/concept-maps-elga2ips.map']
    abs_path_maps = [ os.path.join(path2, rel_path_maps[0]), os.path.join(path2, rel_path_maps[1]), os.path.join(path2, rel_path_maps[2])]

    counter_paths = 0
    for map in abs_path_maps:
        with open(map, 'r') as mapString: # Maps öffnen und als Strings speichern
            data_maps = mapString.read()
        postRequest_maps = requests.post(url_maps, data=data_maps, headers=headers_maps) # post maps to mapping engine
        if postRequest_maps.status_code == 201: # Status im Terminal printen
            print ('Success: ' + rel_path_maps[counter_paths] + ' posted to mapping engine')
        else:
            print ('Warning: ' + rel_path_maps[counter_paths] + ' could not be posted to mapping engine')
        counter_paths = counter_paths + 1
        #print(postRequest_maps.text) # für Testzwecke
else:
    print("Note: Maps were not posted to mapping engine. Latest uploaded maps on mapping engine will be therefore used.")

# CDAs zu FHIR IPS transformieren
url_transform = url_mapping + '/StructureMap/$transform?source=http://hl7.org/fhir/StructureMap/generate-ips-main'
headers_transform = {'Accept': 'application/fhir+xml;fhirVersion=4.0', 'Content-type': 'application/fhir+xml;fhirVersion=4.0'}
rel_path_source = '../prepared-xml-for-ips/mergedCdasForPatientSummary.xml'

with open(rel_path_source, 'r') as sourceString: # merged CDAs aufmachen und als String speichern
        data_source = sourceString.read()
postRequest_transform = requests.post(url_transform, data=data_source, headers=headers_transform)
if postRequest_transform.status_code == 200: # Status im Terminal printen
    print ('Success: ELGA CDAs were successfully transformed to FHIR IPS')
else:
    print ('Error: Failed to transform ELGA CDA to FHIR IPS')
#print(postRequest_transform.text) # für Testzwecke

# Bundle lokal speichern
bundle_from_request= (postRequest_transform.text).encode('utf-8')
generatedBundle = open('../generated-ips/GeneratedIpsBundle.xml', 'wb')
generatedBundle.write(bundle_from_request) 
generatedBundle.close()

#Generiertes IPS auf FHIR Server (Resource Repository) hochladen (optional -> Konfiguration via post_bundle)
if post_bundle==True:
    url_bundle = url_storage + '/Bundle'
    url_structure = url_storage + '/StructureDefinition'
    url_value = url_storage + '/ValueSet'
    url_code = url_storage + '/CodeSystem'

    headers_bundle = {'Accept-Charset': 'utf-8', 'Accept': 'application/xml', 'User-Agent': 'HAPI-FHIR/5.3.0-SNAPSHOT (FHIR Client; FHIR 4.0.1/R4; apache)', 'Accept-Encoding': 'gzip',  'Content-Type': 'application/xml; charset=UTF-8'}

    # Funktion zur Abspeicherung des generierten Bundles 
    def uploadBundle(validation, http_request):

        if validation == 'no_validation':
            validation_path = ''

        if validation == 'validation':
            validation_path = '/$validate?profile=http://hl7.org/fhir/uv/ips/StructureDefinition/Bundle-uv-ips'
            
            # IPS Bundle profile und andere IPS profiles hochladen (Konfiguration via bundle_validation and post_profiles)
            if post_profiles=='all':  # derzeit nicht zu verwenden
                rel_path_strdef = ['../profiles/StructureDefinition-Bundle-uv-ips.xml', '../profiles/StructureDefinition-AllergyIntolerance-uv-ips.xml','../profiles/StructureDefinition-CodeableConcept-uv-ips.xml','../profiles/StructureDefinition-Coding-uv-ips.xml','../profiles/StructureDefinition-Composition-uv-ips.xml','../profiles/StructureDefinition-Condition-uv-ips.xml','../profiles/StructureDefinition-Device-uv-ips.xml','../profiles/StructureDefinition-Immunization-uv-ips.xml','../profiles/StructureDefinition-Medication-uv-ips.xml','../profiles/StructureDefinition-MedicationStatement-uv-ips.xml','../profiles/StructureDefinition-Observation-results-laboratory-uv-ips.xml','../profiles/StructureDefinition-Observation-results-uv-ips.xml','../profiles/StructureDefinition-Organization-uv-ips.xml','../profiles/StructureDefinition-Patient-uv-ips.xml','../profiles/StructureDefinition-Practitioner-uv-ips.xml','../profiles/StructureDefinition-PractitionerRole-uv-ips.xml','../profiles/StructureDefinition-Specimen-uv-ips.xml']
                rel_path_vs = ['../profiles/valueset-all-languages.xml']
                rel_path_cs = ['../profiles/CodeSystem-absent-unknown-uv-ips.xml', '../profiles/CodeSystem-v3-ietf3066.xml']
            if post_profiles=='bundle':
                rel_path_strdef = ['../profiles/StructureDefinition-Bundle-uv-ips.xml']
            
            if post_profiles=='all' or post_profiles=='bundle':
                # structureDefinitions hochladen
                for structureDefinition in rel_path_strdef:
                    with open(structureDefinition, 'r') as profileString: # StructureDefinition aufmachen und als String speichern
                        data_strdef = profileString.read()
                        #print (structureDefinition) # für Testzwecke
                    postRequest_bundleProfile = requests.post(url_structure, data=data_strdef, headers=headers_bundle)
                #print(postRequest_bundleProfile.status_code) # für Testzwecke

            if post_profiles == 'all': # derzeit nicht zu verwenden
                # valueSets hochladen
                for valueSet in rel_path_vs:
                    with open(valueSet, 'r') as profileString: # ValueSets aufmachen und als String speichern
                        data_vs = profileString.read()
                        #print (structureDefinition) # für Testzwecke
                    postRequest_bundleProfile = requests.post(url_value, data=data_vs, headers=headers_bundle)
                
                # codeSystems hochladen
                for codeSystem in rel_path_cs: # derzeit nicht zu verwenden
                    with open(codeSystem, 'r') as profileString: # codeSystems aufmachen und als String speichern
                        data_cs = profileString.read()
                        #print (structureDefinition) # für Testzwecke
                    postRequest_bundleProfile = requests.post(url_code, data=data_cs, headers=headers_bundle)


        if http_request == 'post':
            with open('../generated-ips/GeneratedIpsBundle.xml', 'r') as bundleString: # generiertes Bundle aufmachen und als String speichern
                data_bundle = bundleString.read()
                data_bundle = data_bundle.encode('utf-8')
            postRequest_bundle = requests.post(url_bundle+validation_path, data=data_bundle, headers=headers_bundle)

        if http_request == 'put':
            # Zu aktualisierendes Bundle auswählen 
            print ("--------------------------------------------------------------------------")
            print ("Type in the bundle id of the IPS which should be updated:")
            bundle_id = input() 
            print ("--------------------------------------------------------------------------")

            # Bundle ID aus FHIR Store in generatedIpsBundle.xml einsetzen
            bundle_tree = etree.parse("../generated-ips/GeneratedIpsBundle.xml") 
            bundle_root = bundle_tree.getroot()
            id_element = etree.Element("id", value=bundle_id)
            bundle_root.append(id_element)
            bundle_tree.write('../generated-ips/GeneratedIpsBundleWithId.xml')

            with open('../generated-ips/GeneratedIpsBundleWithId.xml', 'r') as bundleString: # generiertes Bundle aufmachen und als String speichern
                data_bundle = bundleString.read()
                data_bundle = data_bundle.encode('utf-8')
            postRequest_bundle = requests.put(url_bundle + '/' + bundle_id + validation_path, data=data_bundle, headers=headers_bundle)

        # bei Erfolg Status und Bundle-ID im Terminal ausgeben
        if postRequest_bundle.status_code == 201 or postRequest_bundle.status_code==200: 
            # Response lokal speichern 
            response_from_request= (postRequest_bundle.text).encode('utf-8')
            responseBundle = open('../generated-ips/ResponseIpsBundle.xml', 'wb')
            responseBundle.write(response_from_request) 
            responseBundle.close()

            # Bundle ID im Terminal protokollieren
            bundle_tree = etree.parse("../generated-ips/ResponseIpsBundle.xml") 
            bundle_root = bundle_tree.getroot()
            bundle_id = bundle_root.xpath('//fhir:id/@value', namespaces=ns)
            if postRequest_bundle.status_code==201:
                print('Success: FHIR IPS Bundle successfully uploaded to FHIR Server (' + url_storage + '/Bundle/' + bundle_id[0]+")") 
            if postRequest_bundle.status_code==200:
                print('Success: FHIR IPS Bundle was successfully updated to FHIR Server (' + url_storage + '/Bundle/' + bundle_id[0]+")") 
            
            # Bundle ID in der .txt-Datein speichern  (optional -> Konfiguration via store_bundleId)
            if store_bundleId == True:
                uploaded_id = open('../generated-ips/UploadedBundleId.txt', 'w')
                uploaded_id.write(bundle_id[0]) 
                uploaded_id.close()

            # UploadLog lokal speichern
            response_from_request= (postRequest_bundle.text).encode('utf-8')
            responseBundle = open('../generated-ips/UploadLog.xml', 'wb')
            responseBundle.write(response_from_request) 
            responseBundle.close()

        else:
            print ('Error: Failed to upload FHIR IPS Bundle to FHIR Server - more details available in UploadLog.xml')
            response_from_request= (postRequest_bundle.text).encode('utf-8')
            responseBundle = open('../generated-ips/UploadLog.xml', 'wb')
            responseBundle.write(response_from_request) 
            responseBundle.close()

    # Fall 1: keine Validierung aktiviert und kein Bundle IPS für Patienten zuvor hochgeladen (Konfiguration via bundle_validation und update_bundle)
    if (bundle_validation == False) and (update_bundle==False):
        uploadBundle('no_validation', 'post')

    # Fall 2: keine Validierung aktiviert und Bundle IPS für Patienten bereits hochgeladen (Konfiguration via bundle_validation und update_bundle)
    if (bundle_validation == False) and (update_bundle==True):
        uploadBundle('no_validation', 'put')

    # Fall 3: Validierung aktiviert und kein Bundle IPS für Patienten zuvor hochgeladen (Konfiguration via bundle_validation und update_bundle)
    if (bundle_validation == True) and (update_bundle==False):
        uploadBundle('validation', 'post')

    # Fall 4: Validierung aktiviert und Bundle IPS für Patienten bereits hochgeladen (Konfiguration via bundle_validation und update_bundle)le)
    if (bundle_validation == True) and (update_bundle==True):
        print('Warning: Most FHIR Stores do not support PUT requests in combination with the $validate function, therefore the bundle will not be validated.')
        uploadBundle('no_validation', 'put')
        