<h2>Einleitung</h2>
Die entwickelte Applikation erlaubt es einem Anwender ein International Patient Summary (IPS) im FHIR Format aus ELGA CDA Dokumenten zu generieren. Das Mapping wird dabei mit Hilfe von der FHIR Mapping Language durchgeführt. 

<h3>Gesamtübersicht</h3>
In der nachfolgenden Abbildung wird eine Übersicht verschaffen, wie das Mapping Service mit den anderen Servern und mit dem Ablageort interagiert. Eine deteillierte Beschreibung zu den Komponenten ist in der [Masterarbeit](https://gitlab.com/elga-gmbh/partner-projekte/elga2ips/-/blob/main/documentation/masterarbeit-adimitrov.txt) inkludiert. 
<br><br>
<img src="img/ips-transformation-overview.png" alt="IPS Transformation Overview">

<h3>Unterstützte Dokumentklassen</h3>
In der nachfolgenden Tabelle werden die unterstützen Dokumentklassen aufgelistet. Zusätzlich wird für jede Dokumentklasse auch ein GitLab-Pfad angeführt, von wo ein beispielhaftes Dokument für die jeweilige Dokumentenklasse gefunden werden kann.

<table class="tg">
<thead>
  <tr>
    <th class="tg-fymr">Dokumentenklasse</th>
    <th class="tg-fymr">CDA Beispiel</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">ELGA Entlassungsbrief Ärztlich</td>
    <td class="tg-0pky">ELGA GmbH &gt; CDA Beispielbefunde &gt; Basisleitfäden_(2.06.2) &gt; <span style="font-weight:bold">ELGA-023-Entlassungsbrief_aerztlich_EIS-FullSupport.xml</span></td>
  </tr>
  <tr>
    <td class="tg-0pky">ELGA Entlassungsbrief Pflege</td>
    <td class="tg-0pky">ELGA GmbH &gt; CDA Beispielbefunde &gt; Basisleitfäden_(2.06.2) &gt; <span style="font-weight:bold;font-style:normal;text-decoration:none">ELGA-033-Entlassungsbrief_Pflege_EIS-FullSupport.xml</span></td>
  </tr>
  <tr>
    <td class="tg-0pky">ELGA Pflegesituationsbericht</td>
    <td class="tg-0pky">ELGA GmbH &gt; CDA Beispielbefunde &gt; Basisleitfäden_(2.06.2) &gt; <span style="font-weight:bold;font-style:normal;text-decoration:none">ELGA-123-Pflegesituationsbericht_EIS-FullSupport.xml</span></td>
  </tr>
  <tr>
    <td class="tg-0pky">ELGA Laborbefund</td>
    <td class="tg-0pky">ELGA GmbH &gt; CDA Beispielbefunde &gt; Basisleitfäden_(2.06.2) &gt; <span style="font-weight:bold;font-style:normal;text-decoration:none">ELGA-043-Laborbefund_EIS-FullSupport.xml</span></td>
  </tr>
  <tr>
    <td class="tg-0pky">ELGA e-Medikation</td>
    <td class="tg-0pky">ELGA GmbH &gt; CDA Beispielbefunde &gt; Basisleitfäden_(2.06.2) &gt; <span style="font-weight:bold;font-style:normal;text-decoration:none">ELGA-083-e-Medikation_5-Medikationsliste.xml</span></td>
  </tr>
  <tr>
    <td class="tg-0pky">e-Impfpass</td>
    <td class="tg-0pky">ELGA GmbH &gt; CDA Beispielbefunde &gt; e-Impfpass &gt; <span style="font-weight:bold;font-style:normal;text-decoration:none">eImpf-Kompletter_Immunisierungsstatus.xml</span></td>
  </tr>
</tbody>
</table>  


<h2>Installationsanleitung</h2>

Es werden zwei Möglichkeiten aufgezeigt, wie die Generierung durchgeführt werden kann:
<ol>
<li><a href="#ips-generierung-über-python-skript">IPS-Generierung über Python-Skript</a></li>
<li><a href="#ips-generierung-über-http-skript">IPS-Generierung über HTTP-Skript</a></li>
</ol>

<h3>Systemvoraussetzungen</h3>

Im Zuge dieses Projekts wurde Visual Studio Code ([Download](https://code.visualstudio.com/docs/setup/setup-overview)) als Code-Editor ausgewählt, da dieses viele nützliche Erweiterungen anbietet. Python wurde im VSC zum entwickeln des Mapping Services verwendet, nachdem das [dazugehörige Plugin](https://marketplace.visualstudio.com/items?itemName=ms-python.python) installiert wurde. Als Virtual Environment wurde [Anaconda](https://www.anaconda.com) ausgewählt, da mit dieser sich die benötigten Module für das MappingService einfach installieren ließen.

Folgende Python Module werden für die Ausführung des Mapping Services benötigt:

- requests
- ElementTree
- array
- etree
- os

Folgende Visual Studio Erweiterungen wurden während der Entwicklung verwendet:

- Python
- Pylance
- REST Client
- FHIR tools
- FHIR Mapping Language

**Hinweis:** Es sind nicht alle Erweiterungen verplfichtend zum Starten der Anwendung. Die Python Module, sowie die Python Erweiterung selbst sind nur für die erste Generierungsoption notwendig. Für die zweite Generierungsoption reicht lediglich der REST Client aus. 



<h3>Verwendung von Docker Container</h3>

Die Verwendung von Docker ([Download](https://www.docker.com/)) ist für die Ausführung des Mappings nicht verpflichtend. Einfachheit halber können auch öffentliche FHIR Server verwendet werden. Der Anwender kann im Python-Skript bzw. im HTTP-Skript selbst einstellen, welche Endpunkte für die Generierung angesprochen werden sollen. Es wird jedoch empfohlen, dass lokale Docker Container verwendet werden, weil öffentliche FHIR Server nicht immer verlässlich funktionieren.

Folgende Docker Container können optional lokal eingerichtet werden:
<table class="tg">
<thead>
  <tr>
    <th class="tg-fymr">FHIR Server</th>
    <th class="tg-fymr">Docker Container Quelle</th>
    <th class="tg-1wig">Default-Endpunkt</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">loving_hamilton HAPI FHIR Server</td>
    <td class="tg-0pky">https://hub.docker.com/r/hapiproject/hapi</td>
    <td class="tg-0lax">http://localhost:8080/fhir</td>
  </tr>
  <tr>
    <td class="tg-0pky">matchbox HAPI FHIR Server</td>
    <td class="tg-0pky">https://github.com/ahdis/matchbox/</td>
    <td class="tg-0lax">http://localhost:8080/r4</td>
  </tr>
</tbody>
</table>

Folgende öffentliche FHIR Server können alternativ verwendet werden: 
<table class="tg">
<thead>
  <tr>
    <th class="tg-fymr">FHIR Server</th>
    <th class="tg-fymr">Endpunkt</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">HAPI FHIR Server</td>
    <td class="tg-0pky">http://hapi.fhir.org/baseR4</td>
  </tr>
  <tr>
    <td class="tg-0pky">matchbox HAPI FHIR Server</td>
    <td class="tg-0pky">http://test.ahdis.ch/r4</td>
  </tr>
  <tr>
    <td class="tg-0pky">Firely FHIR Server</td>
    <td class="tg-0pky">https://server.fire.ly/r4</td>
  </tr>
</tbody>
</table>

<h3>IPS-Generierung über Python-Skript</h3>

Folgende Schritte sind durchzuführen für die Generierung des IPS über das Python-Skript:

1. Klonen Sie dieses GitLab-Projekt auf die gewünschte Instanz.
2. Legen Sie im Verzeichnis `/mapping-service/input-xmls` alle ELGA CDA Dokumente ab, welche für die Generierung des IPS verwendet werden sollen.
3. Führen Sie das Python-Skript `mapping-service.py` über Visual Studio Code aus dem `mapping-service`-Ordner aus. Alternativ könnte das Mapping Service über das Terminal direkt gestartet werden. 
<br><img width="250" src="img/run-mapping-service.png" alt="Run Mapping-Service">
4. Bedienen Sie das Mapping Service über das Terminal. Auf Basis der <a href="#mögliche-konfiguration">eingestellten Konfiguration</a> kann sich der Ablauf der Applikation durchhaus unterscheiden.
<br><img width="500" src="img/terminal-mapping-service.png" alt="Terminal Mapping Service">
5. Abschließend können Sie das generierte IPS über den im Terminal ausgegebenen Link abrufen.
<br><img width="600" src="img/generated-ips-mapping-service.png" alt="Generated IPS from Mapping Service">


**Hinweis:** Sollte der FHIR Store einen Fehler retournieren, wird dieser im UploadLog.xml (siehe `mapping-service/generated-ips`-Verzeichnis) protokolliert.

<h4>Mögliche Konfigurationen</h4>
Es können folgende Konfigurationen im Mapping Service eingestellt werden, welche sich nachfolgend dementsprechend auf den Mapping Workflow auswirken:

- **FHIR Store Endpunkt:** über zwei Variablen (`fhir_store_endpoints` und `url_storage`) lässt sich der Endpunkt des FHIR Stores definieren (z.B. `http://hapi.fhir.org/baseR4`)
- **Mapping Engine Endpunkt:** über zwei Variablen (`mapping_engine_endpoints` und `url_mapping`) lässt sich der Endpunkt der Mapping Engine definieren (z.B. `https://test.ahdis.ch/r4`)
- **Patientenüberprüfung:** es kann mittels Schalter (`patient_verification`) entschieden werden, ob von dem Mapping Service überprüft werden soll, ob alle Quell-Dokumente zum gleichen Patienten gehören (Optionen: `True`/`False`)
- **Validierung:** es kann mittels Schalter (`bundle_validation`) entschieden werden, ob das generierte Bundle gegen das IPS-Bundle-Profil validiert werden soll (Optionen: `True`/`False`)
- **Bundle Persistenz:** es kann mittels Schalter (`post_bundle`) entschieden werden, ob das generierte Bundle auf den definierten FHIR Store persistiert werden soll (Optionen: `True`/`False`)
- **Bundle Update:** es kann mittels Schalter (`update_bundle`) entschieden werden, ob das generierte Bundle mit Hilfe einer Benutzereingabe, ein bereits existierendes Bundle überschreiben soll (Optionen: `True`/`False`)
- **Map Upload:** es kann mittels Schalter (`post_maps`) entschieden werden, ob die Maps auf die Mapping Engine in Form von StructureDefinitions hochgeladen werden sollen (Optionen: `True`/`False`)
- **Profiles Upload:** es kann mittels String (`post_profiles`) entschieden werden, ob das IPS-Bundle-Profil auf den FHIR Store hochgeladen werden soll (Optionen: `bundle`/`no`)
- **Bundle ID Persistenz:** es kann mittels Schalter (`store_bundleId`) entschieden werden, ob die Bundle ID des persistierten IPS-Bundles lokal in einer txt-Datei für Testzwecke gespeichert werden soll (Optionen: `True`/`False`)


Eine beispielhafte Konfiguration des Mapping Services (`mapping-service.py`) könnte folgendermaßen ausschauen:
```python
fhir_store_endpoints = [’http://hapi.fhir.org/baseR4’,’http://localhost:8080/fhir’,
    ’https://server.fire.ly’, ’http://localhost:8080’, ’https://server.fire.ly/r4’]
mapping_engine_endpoints = [’https://test.ahdis.ch/r4’,’http://localhost:8080/r4’]
url_storage = fhir_store_endpoints[1]
url_mapping = mapping_engine_endpoints[0]
patient_verification = True
bundle_validation = False
post_bundle = True
update_bundle = False
post_maps = True
post_profiles = 'bundle'
store_bundleId = False
```

**Wichtig:** Falls `bundle_validation` auf `True` und `post_profiles` auf `bundle` gesetzt wurden, dann muss im `mapping-service/profiles`-Ordner das Profil für das IPS-Bundle manuell hinzugefügt werden (Dateiname muss `StructureDefinition-Bundle-uv-ips.xml` entsprechen). Zum derzeitigen Zeitpunkt kann das Profil für das [IPS-Bundle aus dem IPS Connectathon Branch](http://build.fhir.org/ig/HL7/fhir-ips/branches/connectathon/profiles.html) entnommen werden.



<h3>IPS-Generierung über HTTP-Skript</h3>

Einfachheitshalber kann die Transformation mittels einem HTTP-Skript durchgeführt werden. In diesem Fall würde es ausreichen, wenn das HTTP-Skript mit dem Plugin "REST Client" ausgeführt wird.

Folgende Schritte sind durchzuführen für die Generierung des IPS über das HTTP-Skript:

1. Klonen Sie dieses GitLab-Projekt auf die gewünschte Instanz.
2. Öffnen Sie das HTTP-Skript `execute-ips-maps.http` aus dem `test-requests`-Ordner.
3. Setzen Sie bei `@host` den Endpunkt der verwendeten Mapping Engine (FHIR Server).
4. Laden Sie alle drei Maps am definierten FHIR Server hoch, indem Sie die Abfragen zw. Zeile 4 und Zeile 23 abschicken.
5. Mit der letzten Abfrage führen Sie die eigentliche Generierung des IPS durch. Standardmäßig wird aus einem bereits im Vorhinein definierten Inputs die Generierung durchgeführt.
<br><img width="450" src="img/http-script-input.png" alt="HTTP-Script Input">
6. Abschließend können Sie das generierte IPS in der Response einsehen.
<br><img width="600" src="img/generated-ips-http-script.png" alt="Generated IPS from HTTP-Script">

**Anmerkung:** Für Testzwecke wurden auch andere HTTP-Skripten angelegt (z.B. zur Validierung, etc.), welche ebenfalls im gleichen Ordner (`test-requests`) vorzufinden sind.


<h2>Links für weitere Informationen</h2>
Unter den nachfolgenden Links können genauere Informationen zu verwendeten Spezifikationen gefunden werden. Zusätzlich wurde das GitHub-Projekt von ahdis aufgelistet, welches als Vorbildprojekt für Transformationen mittels der FHIR Mapping Language gilt. 

- [HL7 FHIR](http://www.hl7.org/fhir/)
- [FHIR Mapping Language](https://www.hl7.org/fhir/mapping-language.html)
- [HL7 IPS IG](http://hl7.org/fhir/uv/ips/)
- [CDA FHIR Maps](https://github.com/hl7ch/cda-fhir-maps)



